# README #

Questa è un'applicazione di esempio (Visual Studio 2013) per visualizzare gli eventi che il LiveOperator Windows può inviare ad un applicativo esterno.  
Il LiveOperator invia un messaggio WM_COPYDATA ad una finestra con nome **LivecareStatusLogger**.  
Il messaggio è composto da una stringa in formato json. Gli eventi loggati sono:
> 1. login
> 2. logoff
> 3. chatbegin
> 4. chatend

Per ogni evento vengono riportate alcune informazioni; di base avremo al tipologia di evento ed un timestamp.

### login ###
L'evento viene notificato quando l'Operatore ha completato la fase di login al server di Livecare. Le informazioni presenti sono il nick dell'operatore ed il suo nome completo.  
Il formato json è il seguente:  
{  
   "event": "login",  
   "timestamp": 1464077940,  
   "nick": "201@assistenza",  
   "fullname": "Giovanni Rossi"  
}  

### logoff ###
L'evento viene notificato quando l'Operatore si disconnette dal server di Livecare. Le informazioni presenti sono le stesse del login.  
Il formato json è il seguente:  
{  
   "event": "logoff",  
   "timestamp": 1464077953,  
   "nick": "201@assistenza",  
   "fullname": "Giovanni Rossi"  
}  

### chatbegin ###
L'evento viene notificato quando un Utente inizia una sessione di Assistenza con l'Operatore. Le informazioni presenti sono il nick, il nome completo, il nome dell'Azienda dell'Utente.  
Il formato json è il seguente:  
{  
   "event": "chatbegin",  
   "timestamp": 1464077945,  
   "nick": "**201#utente1#0",  
   "fullname": "Matteo Verdi",  
   "companyname": "Icona SRL"  
}

### chatend ###
L'evento viene notificato quanto termina una sessione di Assistenza tra Utente ed Operatore.  
Il formato json è il seguente:  
{  
   "event": "chatend",  
   "timestamp": 1464077950,  
   "nick": "**201#utente1#0",  
}

---------------
### Contatti ###
Per qualsiasi informazione aggiuntiva scrivere a support@livecare.it