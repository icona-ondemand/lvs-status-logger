
// StatusLoggerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "StatusLogger.h"
#include "StatusLoggerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include <string>
#include <sstream>
using namespace std;

// CStatusLoggerDlg dialog



CStatusLoggerDlg::CStatusLoggerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CStatusLoggerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CStatusLoggerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_list);
}

BEGIN_MESSAGE_MAP(CStatusLoggerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_COPYDATA()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CStatusLoggerDlg message handlers

BOOL CStatusLoggerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	doLayout(0, 0);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CStatusLoggerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CStatusLoggerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL CStatusLoggerDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	int size;
	char buffer[8192];
	char* finger = (char*)pCopyDataStruct->lpData;

	memcpy(&size, finger, sizeof(int));
	finger += sizeof(int);
	memcpy(buffer, finger, size);
	buffer[size] = '\0';
	finger += size;
	
	m_list.InsertString(-1, (LPCSTR)buffer);
	//scroll to bottom
	int nCount = m_list.GetCount();
	if (nCount > 0)
		m_list.SetCurSel(nCount - 1);

	//remove selection
	m_list.SetCurSel(-1);

	return CDialogEx::OnCopyData(pWnd, pCopyDataStruct);
}


void CStatusLoggerDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	doLayout(cx, cy);
}


void CStatusLoggerDlg::doLayout(int deltaX, int deltaY)
{
	int w, h;

	if (deltaX == 0 && deltaY == 0) {
		w = 463;
		h = 293;
	} else {
		w = deltaX - 20;
		h = deltaY - 20;
	}
	m_list.SetWindowPos(NULL, 10, 10, w, h, SWP_NOZORDER);
	RedrawWindow();
}